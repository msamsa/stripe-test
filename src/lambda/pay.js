import stripePackage from 'stripe';

const stripe = stripePackage('rk_test_oItpePnEbIp4V7amjRr7kxGi');

export function handler(event, context, callback) {
    stripe.charges.create({
        amount: 3000,
        currency: "eur",
        source: "tok_mastercard", // obtained with Stripe.js
        description: "Charge for andrew.davis@example.com"
      }, (err, charge) => {
        console.log(err);
        console.log(charge);
      });
  callback(null, {
    statusCode: 200,
    body: JSON.stringify({msg: "Hello, World!"})
  })
}